import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-spell-gallery',
  templateUrl: './spell-gallery.component.html',
  styleUrls: ['./spell-gallery.component.scss']
})
export class SpellGalleryComponent implements OnInit {

 @Input() listSpells: any = []

  constructor() { }

  ngOnInit(): void {
  }

}
