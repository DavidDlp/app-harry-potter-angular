import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpellGalleryComponent } from './spell-gallery.component';

describe('SpellGalleryComponent', () => {
  let component: SpellGalleryComponent;
  let fixture: ComponentFixture<SpellGalleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpellGalleryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpellGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
