import { Component, OnInit } from '@angular/core';
import { CharactersServiceService } from 'src/app/shared/services/characters-service.service';
import { SpellServiceService } from 'src/app/shared/services/spell-service.service';

@Component({
  selector: 'app-spells',
  templateUrl: './spells.component.html',
  styleUrls: ['./spells.component.scss']
})
export class SpellsComponent implements OnInit {

  public listSpells: any = []

  constructor(private spellService: SpellServiceService) { }

  ngOnInit(): void {
    this.spellService.getSpells().subscribe((spells) => {
      this.listSpells = spells
      console.log(spells)
    }) 
  }

}
