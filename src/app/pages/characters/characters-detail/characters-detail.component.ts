import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharactersServiceService } from 'src/app/shared/services/characters-service.service';

@Component({
  selector: 'app-characters-detail',
  templateUrl: './characters-detail.component.html',
  styleUrls: ['./characters-detail.component.scss']
})
export class CharactersDetailComponent implements OnInit {
  
  public characterId: any = null
  public character: any = {}

  constructor(private route: ActivatedRoute, private charactersService: CharactersServiceService ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params =>{
      this.characterId = Number(params.get('id'))
    })

    this.charactersService.getCharactersById(this.characterId).subscribe((characterApi) =>{ this.character = characterApi
      console.log(this.character)
    })
    
    
  }

}
