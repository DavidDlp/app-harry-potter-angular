import { Component, OnInit } from '@angular/core';
import { CharactersServiceService } from 'src/app/shared/services/characters-service.service';
@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {
 public characters: any =[]

  constructor(private charactersService: CharactersServiceService) { }

  ngOnInit(): void {
    this.charactersService.getCharacters().subscribe( (character) => {
      this.characters = character
      console.log(character)})
  };

}
