import { Component, OnInit } from '@angular/core';
import { BooksServiceService } from 'src/app/shared/services/books-service.service';
import { CharactersServiceService } from 'src/app/shared/services/characters-service.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  public listBooks: any = []

  constructor(private booksService: BooksServiceService) { }

  ngOnInit(): void {
    this.booksService.getBooks().subscribe((book) =>{
      this.listBooks = book
      console.log(book)
    })
  }

}
