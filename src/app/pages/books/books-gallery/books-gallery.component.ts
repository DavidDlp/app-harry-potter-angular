import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-books-gallery',
  templateUrl: './books-gallery.component.html',
  styleUrls: ['./books-gallery.component.scss']
})
export class BooksGalleryComponent implements OnInit {

 @Input () listBooks: any =[]

  constructor() { }

  ngOnInit(): void {
  }

}
