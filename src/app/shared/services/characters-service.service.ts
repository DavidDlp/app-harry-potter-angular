import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CharactersServiceService {

  constructor( private http: HttpClient) { }
    getCharacters(){
      return this.http.get("https://fedeperin-harry-potter-api.herokuapp.com/personajes")
  }

    getCharactersById(CharacterID:number){
      return this.http.get("https://fedeperin-harry-potter-api.herokuapp.com/personajes/" + CharacterID)
  }

}



