import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BooksServiceService {

  constructor(private http: HttpClient) { }

  getBooks(){
    return this.http.get("https://fedeperin-harry-potter-api.herokuapp.com/libros")
  }
}
