import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SpellServiceService {

  constructor( private http: HttpClient ) { }

    getSpells(){
      return this.http.get("https://fedeperin-harry-potter-api.herokuapp.com/hechizos")
      
  }
}
