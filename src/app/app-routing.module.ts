import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { BooksComponent } from './pages/books/books.component';
import { CharactersDetailComponent } from './pages/characters/characters-detail/characters-detail.component';
import { CharactersComponent } from './pages/characters/characters.component';
import { HomeComponent } from './pages/home/home.component';
import { SpellsComponent } from './pages/spells/spells.component';

const routes: Routes = [
  {path:"" , component: HomeComponent},
  {path:"Characters" , component: CharactersComponent},
  {path:"Characters/:id" , component: CharactersDetailComponent},
  {path:"Spells" , component: SpellsComponent},
  {path:"Books" , component: BooksComponent},
  {path:"About" , component: AboutComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
