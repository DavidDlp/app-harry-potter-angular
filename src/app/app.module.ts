import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './core/menu/menu.component';
import { CharactersComponent } from './pages/characters/characters.component';
import { SpellsComponent } from './pages/spells/spells.component';
import { HomeComponent } from './pages/home/home.component';
import { BooksComponent } from './pages/books/books.component';
import { ComponentComponent } from './core/component/component.component';
import { FooterComponent } from './core/footer/footer.component';
import { AboutComponent } from './pages/about/about.component';
import { CharactersGalleryComponent } from './pages/characters/characters-gallery/characters-gallery.component';
import { SpellGalleryComponent } from './pages/spells/spell-gallery/spell-gallery.component';
import { BooksGalleryComponent } from './pages/books/books-gallery/books-gallery.component';
import { HttpClientModule } from '@angular/common/http';
import { CharactersDetailComponent } from './pages/characters/characters-detail/characters-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    CharactersComponent,
    SpellsComponent,
    HomeComponent,
    BooksComponent,
    ComponentComponent,
    FooterComponent,
    AboutComponent,
    CharactersGalleryComponent,
    SpellGalleryComponent,
    BooksGalleryComponent,
    CharactersDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
