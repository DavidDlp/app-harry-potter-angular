export interface CharacterInterface {
    id: number,
    personaje: string,
    apodo: string,
    estudianteDeHogwarts: boolean,
    casaDeHogwarts: string,
    interpretado_por: string,
    hijos: string[],
    imagen: string
}