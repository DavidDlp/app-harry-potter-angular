export interface BookInterface{
    id: number,
    libro: string,
    titulo_original: string,
    fecha_de_lanzamiento: string,
    autora: string,
    descripcion: string
}